from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Categorie(models.Model):
    nom = models.CharField(max_length=30)

    def __str__(self):
        return self.nom

class Etat(models.Model):
    etat = models.CharField(max_length=30)

    def __str__(self):
        return self.nom

class Item(models.Model):
    titre = models.CharField(max_length=50)
    texte = models.TextField()
    categorie = models.ForeignKey(Categorie, on_delete=models.SET_NULL, null=True)
    # etat = models.ForeignKey(Etat, on_delete=models.PROTECT)
    # auteur = models.ForeignKey(User, on_delete=models.PROTECT)
    # date_ajout = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.titre